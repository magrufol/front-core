var config = require('./config/ftp.config');
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    clean = require('gulp-clean'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    gutil = require('gulp-util'),
    ftp = require('vinyl-ftp'),
    browserSync = require('browser-sync');
    sourcemaps = require('gulp-sourcemaps'),
    argv = require('yargs').argv;

if( argv.p === 'local' ){
    var reloadBaseUrl = config.sitesData[argv.p].url;
    var siteRootPath = '../OSPanel/domains/v3/wp-content/themes/leos';
    var deploy = false;

    var sassWatchPath = siteRootPath + '/assets/styles/**/*.sass';
    var jsWatchPath = siteRootPath + '/assets/scripts/**/*.js';
    var distPath = siteRootPath + '/dist';
}else if((argv.p === 'customle') || (argv.p === 'gishot')) {
    var reloadBaseUrl = config.sitesData[argv.p].url;
    var siteRootPath = '../Sites/' + argv.p + '/www/wp-content/themes/' + argv.p;;
    var deploy = true; 

    var sassWatchPath = siteRootPath + '/assets/styles/**/*.sass';
    var jsWatchPath = siteRootPath + '/assets/scripts/*.js';
    var distPath = siteRootPath + '/dist';
}else{
    var reloadBaseUrl = config.sitesData[argv.p].url;
    var siteRootPath = '../Sites/' + argv.p + '/www/wp-content/themes/' + argv.p;;
    var deploy = true; 

    var sassWatchPath = siteRootPath + '/styles/**/*.sass';
    var jsWatchPath = siteRootPath + '/scripts/*.js';
    var distPath = siteRootPath + '/dist';
}






gulp.task('default', ['watch'], function () {
    browserSync.init({
        proxy: reloadBaseUrl,
        notify: true
    });
});

gulp.task('watch',['sass', 'js'], function() {
    gulp.watch(sassWatchPath, ['sass']);
    gulp.watch(jsWatchPath, ['js']);
    if(deploy){
        gulp.watch(siteRootPath + '/dist/**', ['deploy']);
    }
    gulp.watch(siteRootPath + '/**/*.php', ['reload']);
});


gulp.task('reload', function () {
    browserSync.reload();
});

gulp.task('sass', function () {
    return gulp.src(sassWatchPath)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expand',
            includePaths: [
                'node_modules/bootstrap/scss',
                'node_modules/swiper/dist/css',
                'node_modules/hover.css/css',
                'node_modules/animate.css',
                'node_modules/hamburgers/_sass/hamburgers',
                'node_modules/izimodal/css',
            ]
        }).on('error', notify.onError()))
        .pipe(rename({suffix: '.min', prefix: ''}))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(siteRootPath + '/dist/css' ))
});

gulp.task('js', function () {
    return gulp.src([
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/izimodal/js/iziModal.min.js',
        'node_modules/swiper/dist/js/swiper.min.js',
        'node_modules/wowjs/dist/wow.js',
        'node_modules/jquery-match-height/dist/jquery.matchHeight-min.js',
        jsWatchPath
    ])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(siteRootPath + '/dist/js'));
});

gulp.task('deploy', function(){
   var connection = ftp.create({
       host: config.sitesData[argv.p].host,
       user: config.sitesData[argv.p].user,
       password: config.sitesData[argv.p].password,
       parallel: 10,
       log: gutil.log
   });

    var globs = [siteRootPath + '/dist/**'];
    return gulp.src(globs, {buffer: false})
        .pipe(connection.dest('/public_html/wp-content/themes/' + argv.p +'/dist/').on('end', function () {
            browserSync.reload();
        }));

});